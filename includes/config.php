<?php

spl_autoload_register(function ($class_name) {
    include_once($_SERVER["DOCUMENT_ROOT"] . "\/class\/" . $class_name . ".php");
});

session_start();

try {
    $connect = new PDO("sqlite:../sqlite.db");
    $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (\PDOException $e) {
    echo 'Erreur sql : ' . $e->getMessage();
}

if(isset($_COOKIE['user']) && !empty($_SESSION['user'])){
    $user = $connect->query('SELECT idUsers,email,name,lastname,imageUrl,updated,admin 
                            FROM Users 
                            WHERE email = '.$connect->quote($_COOKIE['user']))->fetch();
    $_SESSION['user']=$user;
}
