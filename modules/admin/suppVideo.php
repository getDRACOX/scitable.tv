<?php
    if(!empty($_POST)&&isset($_POST['video'])){
        $connect->beginTransaction();
        try {
            $sth = $connect->prepare("DELETE FROM Content
                                        WHERE idContent = ".$connect->quote($_POST['video']));
            $sth->execute();

            //TODO supprimer l'image correspondante dans le doc img

            $connect->commit();
            $_SESSION['videoSupp'] = true;
        } catch (\Throwable $th) {
            $connect->rollBack();
            $_SESSION['videoSupp'] = false;
        }
    }

    function addOption($connect){
        $videos = $connect->query("SELECT idContent,name FROM Content")->fetchAll();

        if($videos){
            $string = "";
            var_dump($videos);
            foreach($videos as $video){
                $string .= '<option value="'.$video['idContent'].'">'.$video['name'].'</option>';
            }
            return $string;
        }
    }
?>

<div id="back-title">
    <a href="/admin.php">retour</a>
    <h1>Supprimer une vidéo</h1>
</div>

<form id="suppVideo" method="post" enctype="multipart/form-data">
    <select name="video" id="videolist">
        <?=addOption($connect)?>
    </select>
    <button type="submit">Supprimer</button>
</form>