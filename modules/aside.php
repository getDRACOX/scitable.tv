<?php 

function adminAccess(){
    if($_SESSION['user']['admin'] == 1){
        return '<li><a href="/admin.php">Panneau Admin</a></li>';
    }
}

function addImageAside($_imageUrl)
{
    if ($_imageUrl == null) {
        $_imageUrl = "profil.png";
    }
    return '<img src="/img/' . $_imageUrl . '" alt="profil">';
}

?>

<aside id="aside">
    <nav>
        <div id="imageUser">
            <?= addImageAside($_SESSION['user']['imageUrl']); ?>
        </div>
        <ul>
            <li><a href="/home.php">Accueil</a></li>
            <li><a href="/category.php">Catégories</a></li>
            <li><a href="/profil.php">Mon profil</a></li>
            <?= adminAccess()?>
            <li><a href="/logout.php">Déconnexion</a></li>
        </ul>
    </nav>
    <div>
        <p>© IUT Lyon 1</p>
        <p>SCITABLE - 2021</p>
    </div>
</aside>