<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/includes/config.php");
if (!isset($_SESSION['user']['admin']) || $_SESSION['user']['admin'] != 1) {
    header('Location: /home.php');
    exit();
}
?>

<!DOCTYPE html>
<html lang="fr">

<?php
$pageName = "Scitable.TV - ADMIN";
include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/head.php");
?>

<body>

    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/header.php"); ?>

    <main id="admin-page">

    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/aside.php"); ?>

        <?php
            if(isset($_GET['action'])){
                switch($_GET['action']){
                    case 'add':
                        include_once($_SERVER['DOCUMENT_ROOT'] . '/modules/admin/addVideo.php');
                        break;
                    case 'supp':
                        include_once($_SERVER['DOCUMENT_ROOT'] . '/modules/admin/suppVideo.php');
                        break;
                    case 'userlist':
                        include_once($_SERVER['DOCUMENT_ROOT'] . '/modules/admin/userlist.php');
                        break;
                    default:
                        include_once($_SERVER['DOCUMENT_ROOT'] . '/modules/admin/admin_page.html');
                }
            }
            else{
                include_once($_SERVER['DOCUMENT_ROOT'] . '/modules/admin/admin_page.html');
            }
        ?>

    </main>


    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/footer.php"); ?>

</body>

<script>
    <?php
        if(isset($_SESSION['video'])){
            if($_SESSION['video']){
                echo '
                function videoUpload(){
                    alert("La video a bien été ajouté au site");
                }
            
                videoUpload();
                ';
            }else{
                echo '
                function videoUpload(){
                    alert("Error : La video n\'a pas été ajouté au site");
                }
            
                videoUpload();
                ';
            }
            unset($_SESSION['video']);
        }

        if(isset($_SESSION['videoSupp'])){
            if($_SESSION['videoSupp']){
                echo '
                function videoUpload(){
                    alert("La video a bien été supprimé du site");
                }
            
                videoUpload();
                ';
            }else{
                echo '
                function videoUpload(){
                    alert("Error : La video n\'a pas été supprimé du site");
                }
            
                videoUpload();
                ';
            }
            unset($_SESSION['videoSupp']);
        }
    ?>
    
</script>
<script src="js/action.js"></script>

</html>