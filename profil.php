<?php
include 'includes/config.php';
if (empty($_SESSION['user']) || !isset($_SESSION['user'])) {
    header('Location: /index.php');
    exit();
}

/*Traitement en cas d'enregistrement*/
if (!empty($_POST) && isset($_POST['name'])) {
    /**
     * enregistrement de la nouvelle image
     * suppression de l'ancienne 
     */
    if (!empty($_FILES)) {
        $date = date('YmdHis');
        $type = explode('/', $_FILES["image"]['type'])[1];
        move_uploaded_file($_FILES["image"]['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . "/img/profil" . $date . "." . $type);
        if (file_exists(__DIR__ . '/img/' . $_SESSION['user']['imageUrl']) && $_SESSION['user']['imageUrl'] != "profil.png") {
            unlink(__DIR__ . '/img/' . $_SESSION['user']['imageUrl']);
        }
    }
    $connect->beginTransaction();
    try {

        $statement = $connect->prepare('UPDATE Users
                SET name = :name, 
                lastname = :lastname, 
                email = :email,
                imageUrl = :image
                 WHERE idUsers = :id');

        $statement->execute(
            [
                'name' => $_POST['name'],
                'lastname' => $_POST['lastname'],
                'email' => $_POST['email'],
                'image' => "profil" . $date . "." . $type,
                'id' => $_SESSION['user']['idUsers']
            ]
        );

        $_SESSION['user']['name'] = $_POST['name'];
        $_SESSION['user']['lastname'] = $_POST['lastname'];
        $_SESSION['user']['email'] = $_POST['email'];
        $_SESSION['user']['imageUrl'] = "profil" . $date . "." . $type;

        $connect->commit();

        /*Destruction de POST dans le cas d'une actualisation de page*/
        unset($_POST);
        header("Location: " . $_SERVER['PHP_SELF']);
        exit();
    } catch (\Throwable $th) {
        $connect->rollBack();
        echo $th->getLine() . " " . $th->getMessage();
    }
}


function addImage($_imageUrl)
{
    if ($_imageUrl == null) {
        $_imageUrl = "profil.png";
    }
    return '<img src="/img/' . $_imageUrl . '" alt="profil">';
}

function addPersonalInformations()
{

    return '
        <input type="text" name="name" id="name" value="' . $_SESSION['user']['name'] . '" disabled>
        <input type="text" name="lastname" id="lastname" value="' . $_SESSION['user']['lastname'] . '" disabled>
        <input type="email" name="email" id="email" value="' . $_SESSION['user']['email'] . '" disabled>
    ';
}


?>


<!DOCTYPE html>
<html lang="fr">

<?php
$pageName = "Scitable.TV - PROFIL";
include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/head.php");
?>

<body>

    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/header.php"); ?>

    <main id="profil-page">

        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/aside.php"); ?>

        <section>
            <div id="title">
                <h1 id="profil-title">MY PROFIL</h1>
            </div>

            <form method="post" enctype="multipart/form-data">
                <div>
                    <div id="photo">
                        <?= addImage($_SESSION['user']['imageUrl']); ?>
                        <input type="file" name="image" id="image" disabled>
                    </div>
                    <div id="personal-informations">
                        <?= addPersonalInformations();
                        ?>
                    </div>
                </div>

                <button type="button" id="modify">modifier</button>
                <button type="submit" id="save" disabled>enregister</button>
            </form>

            <div id="recommandation">
                <h2></h2>
                <!--//TODO-->
                
                <p><?php 

                $pyout = shell_exec('pyreco.py');
               
                var_dump($pyout);
                
                echo "\n";

                ?>
                </p>
            </div>

        </section>

    </main>


    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/modules/footer.php"); ?>

</body>

<script>
    let modifyActive = false;

    let saves = new Array();

    function modifyProfile() {
        let content = document.querySelectorAll('#personal-informations input');

        if (modifyActive) {
            let inputs = document.querySelectorAll('#profil-page section form input');
            document.getElementById('save').setAttribute('disabled', true);
            for (let index = 0; index < inputs.length; index++) {
                inputs[index].setAttribute('disabled', true);
            }
            this.textContent = "modifier";
            for (let index = 0; index < content.length; index++) {
                content[index].value = saves[index];
            }
            saves = new Array();
            modifyActive = false;
        } else {

            let inputs = document.querySelectorAll('#profil-page section form input');
            document.getElementById('save').removeAttribute('disabled');
            for (let index = 0; index < inputs.length; index++) {
                inputs[index].removeAttribute('disabled');
            }
            this.textContent = "annuler";

            for (let index = 0; index < content.length; index++) {
                saves.push(content[index].value);
            }
            modifyActive = true;
        }
    }

    document.getElementById('modify').addEventListener("click", modifyProfile);
</script>
<script src="js/action.js"></script>

</html>