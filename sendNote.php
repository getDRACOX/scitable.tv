<?php

include_once($_SERVER['DOCUMENT_ROOT'] . "/includes/config.php");

$uid = $_POST["uid"];
$vid = $_POST["vid"];
$n = $_POST["note"]??NULL;

if($n >= 0 || $n <= 5){
    $connect->beginTransaction();
    try {
        
        $sth = $connect->prepare("INSERT OR REPLACE INTO Rate VALUES (:value,:idUser,:idContent);");
        $sth->execute(
            [
                'value' => $n,
                'idUser' => $uid,
                'idContent' => $vid
            ]
        );

        $connect->commit();
    } catch (\Throwable $th) {
        $connect->rollBack();
    }
    $noteAVG = $connect->query('SELECT avg(value) FROM Rate WHERE idContent = '.$vid)->fetchColumn();
    echo $n."-".$noteAVG;
}
else{
    echo "error-error";
}