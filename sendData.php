<?php

include_once($_SERVER['DOCUMENT_ROOT'] . "/includes/config.php");

function inverse($bool){
    if($bool=="1"){
        return "0";
    }
    return "1";
}

$id = $_POST["id"];
$a = strval($_POST["admin"]??NULL);
$b = strval($_POST["blocked"]??NULL);
if($a == "0" || $a == "1"){
    $a = inverse($a);
    $connect->beginTransaction();
    try {
        
        $sth = $connect->prepare("UPDATE Users SET admin = $a WHERE idUsers = $id");
        $sth->execute();

        $connect->commit();
    } catch (\Throwable $th) {
        $connect->rollBack();
    }
    echo $a?"unadmin":"admin";
}
else if($b == "0" || $b == "1"){
    $b = inverse($b);
    $connect->beginTransaction();
    try {
        
        $sth = $connect->prepare("UPDATE Users SET active = $b WHERE idUsers = $id");
        $sth->execute();

        $connect->commit();
    } catch (\Throwable $th) {
        $connect->rollBack();
    }
    echo $b?"block":"unblock";
}

